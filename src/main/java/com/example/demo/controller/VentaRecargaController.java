/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.controller;

import com.example.demo.model.VentaRecarga;
import com.example.demo.service.VentaRecargaService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */
@RestController
@RequestMapping("/api/ventas")
public class VentaRecargaController {

    @Autowired
    private VentaRecargaService ventaRecargaService;

    @PostMapping
    public ResponseEntity<?> crearVenta(@RequestBody VentaRecarga ventaRecarga) {
        try {
            VentaRecarga nuevoVentaRecarga = ventaRecargaService.realizarVenta(ventaRecarga);
            return new ResponseEntity<>(nuevoVentaRecarga, HttpStatus.CREATED);
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body(Map.of("response", "Error: El elemento único ya existe (por ejemplo, email duplicado)."));
        } catch (Exception e) {
            return new ResponseEntity<>(Map.of("response", "Error interno del servidor"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((org.springframework.validation.FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return new ResponseEntity<>(Map.of("response", errors), HttpStatus.BAD_REQUEST);
    }

    @GetMapping
    public ResponseEntity<List<VentaRecarga>> listarVentas() {
        try {
            List<VentaRecarga> listaVentas = ventaRecargaService.listarVentas();
            if (listaVentas.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(listaVentas, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/buscar-venta")
    public ResponseEntity<VentaRecarga> buscarVenta(@RequestParam String uuid) {
        try {
            VentaRecarga ventaRecarga = ventaRecargaService.buscarVenta(uuid);
            return ventaRecarga != null ? new ResponseEntity<>(ventaRecarga, HttpStatus.OK)
                    : new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/buscar-venta-cliente")
    public ResponseEntity<List<VentaRecarga>> buscarVentaPorCliente(@RequestParam String clienteUuid) {
        try {
            List<VentaRecarga> listaVentas = ventaRecargaService.buscarVentasCliente(clienteUuid);
            if (listaVentas.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(listaVentas, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/buscar-venta-vendedor")
    public ResponseEntity<List<VentaRecarga>> buscarVentaPorVendedor(@RequestParam String vendedorUuid) {
        try {
            List<VentaRecarga> listaVentas = ventaRecargaService.buscarVentasVendedor(vendedorUuid);
            if (listaVentas.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(listaVentas, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/buscar-venta-operador")
    public ResponseEntity<List<VentaRecarga>> buscarVentaPorOperador(@RequestParam String operadorUuid) {
        try {
            List<VentaRecarga> listaVentas = ventaRecargaService.buscarVentasOperador(operadorUuid);
            if (listaVentas.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(listaVentas, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/buscar-venta-vendedor-operador")
    public ResponseEntity<List<VentaRecarga>> buscarVentasPorVendedorYOperador(@RequestParam String vendedorUuid, @RequestParam String operadorUuid) {
        try {
            List<VentaRecarga> listaVentas = ventaRecargaService.buscarVentas(vendedorUuid, operadorUuid);
            if (listaVentas.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(listaVentas, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/cambiar-estado")
    public ResponseEntity<VentaRecarga> cambioEstado(@RequestParam String ventaUuid, @RequestParam String nuevoEstado) {
        try {
            VentaRecarga ventaActualizada = ventaRecargaService.cambioEstado(ventaUuid, nuevoEstado);
            if (ventaActualizada != null) {
                return new ResponseEntity<>(ventaActualizada, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
