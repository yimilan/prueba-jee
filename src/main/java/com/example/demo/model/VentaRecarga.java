/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.Date;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "sales")
public class VentaRecarga {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(updatable = false, nullable = false)
    private String uuid;

    @Column(name = "datatime_sale")
    @Temporal(TemporalType.DATE)
    private Date datatimeSale;

    @Column(nullable = false)
    @NotNull
    private Long total;

    @Column(length = 45)
    @NotBlank
    private String recharged_phone;

    @Column(length = 45)
    @NotBlank
    private String status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_uuid", insertable = false, updatable = false)
    private Cliente client;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "operator_uuid", insertable = false, updatable = false)
    private Operador operator;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "seller_uuid", insertable = false, updatable = false)
    private Vendedor seller;

    @Column(name = "client_uuid")
    private String client_uuid;

    @Column(name = "operator_uuid")
    private String operator_uuid;

    @Column(name = "seller_uuid")
    private String seller_uuid;

    public String getClient_uuid() {
        return client_uuid;
    }

    public void setClient_uuid(String client_uuid) {
        this.client_uuid = client_uuid;
    }

    public String getOperator_uuid() {
        return operator_uuid;
    }

    public void setOperator_uuid(String operator_uuid) {
        this.operator_uuid = operator_uuid;
    }

    public String getSeller_uuid() {
        return seller_uuid;
    }

    public void setSeller_uuid(String seller_uuid) {
        this.seller_uuid = seller_uuid;
    }

    @PrePersist
    public void prePersist() {
        datatimeSale = new Date();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getDatatimeSale() {
        return datatimeSale;
    }

    public void setDatatimeSale(Date datatimeSale) {
        this.datatimeSale = datatimeSale;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getRecharged_phone() {
        return recharged_phone;
    }

    public void setRecharged_phone(String recharged_phone) {
        this.recharged_phone = recharged_phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Vendedor getSeller() {
        return seller;
    }

    public void setSeller(Vendedor seller) {
        this.seller = seller;
    }

    public Operador getOperator() {
        return operator;
    }

    public void setOperator(Operador operator) {
        this.operator = operator;
    }

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

}
