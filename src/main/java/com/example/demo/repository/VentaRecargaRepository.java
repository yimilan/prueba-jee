/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.demo.repository;

import com.example.demo.model.VentaRecarga;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ADMIN
 */
public interface VentaRecargaRepository  extends CrudRepository<VentaRecarga, String>{
    @Query("SELECT vr FROM VentaRecarga vr WHERE vr.client.uuid = :clienteUuid")
    List<VentaRecarga> findByClienteUuid(@Param("clienteUuid") String clienteUuid);
    
    @Query("SELECT vr FROM VentaRecarga vr WHERE vr.seller.uuid = :vendedorUuid")
    List<VentaRecarga> findByVendedorUuid(@Param("vendedorUuid") String vendedorUuid);
    
    @Query("SELECT vr FROM VentaRecarga vr WHERE vr.operator.uuid = :operadorUuid")
    List<VentaRecarga> findByOperadorUuid(@Param("operadorUuid") String operadorUuid);
    
    @Query("SELECT vr FROM VentaRecarga vr WHERE vr.seller.uuid = :vendedorUuid AND vr.operator.uuid = :operadorUuid")
    List<VentaRecarga> findByVendedorUuidAndOperadorUuid(@Param("vendedorUuid") String vendedorUuid, @Param("operadorUuid") String operadorUuid);


}
