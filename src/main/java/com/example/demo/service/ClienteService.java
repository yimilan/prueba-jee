/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.service;

import java.util.List;
import com.example.demo.model.Cliente;
import com.example.demo.repository.ClienteRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ADMIN
 */
@Service
public class ClienteService implements IClienteService{    
    
    @Autowired
    private  ClienteRepository clienteDao;

    @Override
    @Transactional
    public Cliente crearCliente(Cliente cliente) {
        return clienteDao.save(cliente);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Cliente> listarClientes() {
        return (List<Cliente>) clienteDao.findAll();
    }

    @Override
    public Cliente verCliente(String UUID) {
        return clienteDao.findById(UUID).orElse(null);
    }

    @Override
    public Cliente verClienteEmail(String email) {
        return clienteDao.findByEmail(email);
    }
}
