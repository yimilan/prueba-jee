/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.model.Operador;
import com.example.demo.repository.OperadorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ADMIN
 */
@Service
public class OperadorService implements IOperadorService{
   
    
    @Autowired
    private  OperadorRepository operadorDao;
    
    @Override
    public Operador crearOperador(Operador operador) {
        return operadorDao.save(operador);
    }

    @Override
    public List<Operador> listarOperadores() {
        return (List<Operador>) operadorDao.findAll();
    }

    @Override
    public Operador verOperador(String UUID) {
        return operadorDao.findById(UUID).orElse(null);
    }
    
}
