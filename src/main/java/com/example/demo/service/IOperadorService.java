/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.model.Operador;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface IOperadorService {
    public Operador crearOperador(Operador operador);
    public List<Operador> listarOperadores();
    public Operador verOperador(String UUID);
}
