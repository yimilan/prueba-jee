/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.model.Vendedor;
import com.example.demo.repository.VendedorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ADMIN
 */
@Service
public class VendedorService implements IVendedorService{
   
    
    @Autowired
    private  VendedorRepository vendedorDao;
    
    @Override
    public Vendedor crearVendedor(Vendedor vendedor) {
        return vendedorDao.save(vendedor);
    }

    @Override
    public List<Vendedor> listarVendedores() {
        return (List<Vendedor>) vendedorDao.findAll();
    }

    @Override
    public Vendedor verVendedor(String UUID) {
        return vendedorDao.findById(UUID).orElse(null);
    }
    
}
