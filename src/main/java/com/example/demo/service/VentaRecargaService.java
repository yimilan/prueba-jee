/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.model.Cliente;
import com.example.demo.model.Operador;
import com.example.demo.model.Vendedor;
import com.example.demo.model.VentaRecarga;
import com.example.demo.repository.ClienteRepository;
import com.example.demo.repository.OperadorRepository;
import com.example.demo.repository.VendedorRepository;
import com.example.demo.repository.VentaRecargaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ADMIN
 */
@Service
public class VentaRecargaService implements IVentaRecargaService {

    @Autowired
    private VentaRecargaRepository ventaRecargaDao;
    @Autowired
    private ClienteRepository clienteDao;
    @Autowired
    private OperadorRepository operadorDao;
    @Autowired
    private VendedorRepository vendedorDao;

    @Override
    public VentaRecarga realizarVenta(VentaRecarga ventaRecarga) {
        Cliente cliente = clienteDao.findById(ventaRecarga.getClient_uuid()).orElse(null);
        Operador operador = operadorDao.findById(ventaRecarga.getOperator_uuid()).orElse(null);
        Vendedor vendedor = vendedorDao.findById(ventaRecarga.getSeller_uuid()).orElse(null);

        ventaRecarga.setClient(cliente);
        ventaRecarga.setOperator(operador);
        ventaRecarga.setSeller(vendedor);
        return ventaRecargaDao.save(ventaRecarga);
    }

    @Override
    public List<VentaRecarga> listarVentas() {
        return (List<VentaRecarga>) ventaRecargaDao.findAll();
    }

    @Override
    public VentaRecarga cambioEstado(String UUID, String status) {
        VentaRecarga ventaRecarga = ventaRecargaDao.findById(UUID).orElse(null);
        if (ventaRecarga != null) {
            ventaRecarga.setStatus(status);
            return ventaRecargaDao.save(ventaRecarga);
        }
        return null;
    }

    @Override
    public VentaRecarga buscarVenta(String UUID) {
        return ventaRecargaDao.findById(UUID).orElse(null);
    }

    @Override
    public List<VentaRecarga> buscarVentasCliente(String clienteUuid) {
        return ventaRecargaDao.findByClienteUuid(clienteUuid);
    }

    @Override
    public List<VentaRecarga> buscarVentasVendedor(String vendedorUuid) {
        return ventaRecargaDao.findByVendedorUuid(vendedorUuid);
    }

    @Override
    public List<VentaRecarga> buscarVentasOperador(String operadorUuid) {
        return ventaRecargaDao.findByOperadorUuid(operadorUuid);
    }

    @Override
    public List<VentaRecarga> buscarVentas(String UUIDVendedor, String UUIDOperador) {
        return ventaRecargaDao.findByVendedorUuidAndOperadorUuid(UUIDVendedor, UUIDOperador);
    }

}
