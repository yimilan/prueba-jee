/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.model.Cliente;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface IClienteService {
    public Cliente crearCliente(Cliente cliente);
    public List<Cliente> listarClientes();
    public Cliente verCliente(String UUID);
    public Cliente verClienteEmail(String email);
}
