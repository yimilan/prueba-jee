/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.model.Vendedor;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface IVendedorService {
    public Vendedor crearVendedor(Vendedor vendedor);
    public List<Vendedor> listarVendedores();
    public Vendedor verVendedor(String UUID);
}
