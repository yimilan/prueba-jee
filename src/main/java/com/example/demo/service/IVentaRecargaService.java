/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.demo.service;

import com.example.demo.model.VentaRecarga;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface IVentaRecargaService {
    public VentaRecarga realizarVenta(VentaRecarga ventaRecarga);
    public List<VentaRecarga> listarVentas();
    public VentaRecarga cambioEstado(String UUID, String status);
    /**
     * Buscar Venta especifica
     * @param UUID
     * @return 
     */
    public VentaRecarga buscarVenta(String UUID);
    
    
    /**
     * Buscar Ventas por UUID de Vendedor y Operador
     * @param UUIDVendedor UUID Vendedor
     * @param UUIDOperador UUID Operador
     * @return 
     */    
    public List<VentaRecarga> buscarVentas(String UUIDVendedor, String UUIDOperador);
    
    
    /**
     * Buscar Ventas por UUID de Cliente
     * @param clienteUuid
     * @return 
     */
    public List<VentaRecarga> buscarVentasCliente(String clienteUuid);
    
    
    /**
     * Buscar Ventas por UUID de vendedor
     * @param vendedorUuid
     * @return 
     */
    public List<VentaRecarga> buscarVentasVendedor(String vendedorUuid);
    
    /**
     * Buscar Ventas por UUID de operador
     * @param operadorUuid
     * @return 
     */
    public List<VentaRecarga> buscarVentasOperador(String operadorUuid);
    
}
